{
  imports = [ ./networkmanager ];

  networking.firewall.allowedTCPPorts = [22 3000 3030 3333 5000 5300 8000 8081 8080 8888 9000 19000 19001 19002 ];
}
